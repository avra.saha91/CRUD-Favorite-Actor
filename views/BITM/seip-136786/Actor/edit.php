<?php
include_once '../../../../BITM/Seip-136786/Actor/Actor.php';

$editobject = new Actor();

$editobject->prepare($_GET);
$single = $editobject->show();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>

<html>
    <head>
        <title>
            Actor Name
        </title>

    </head>
    <body>
        <fieldset>
            <legend>Update Actor Name</legend>
            <a href="index.php">Back to list</a>

            <form action="update.php" method="post">
                <label>Update Actor</label>
                <input type="text" name="actor_name" value="<?php echo $single['title'] ?>">
                <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                <input type="submit" value="Update">
            </form>
        </fieldset>
    </body>
</html>





