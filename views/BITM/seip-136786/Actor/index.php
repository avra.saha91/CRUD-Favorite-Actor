<?php
include_once '../../../../BITM/Seip-136786/Actor/Actor.php';
?>

<a href="../../../../Index.php">List of project</a>
<a href="create.php">Add Actor Name</a>
<?php
$obj = new Actor();
$data = $obj->index();
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
    <head>
        <title>Index | Data </title>
    </head>
    <body>
        <table border="5" >
            <tr>
                <td>Id</td>
                <td>Title</td>
                <td colspan="3">  Action   </td>
            </tr>
            <?php
            $serial = 1;
            if (isset($data) && !empty($data)) {

                foreach ($data as $Singledata) {
                    ?>

                    <tr>
                        <td><?php echo $serial++ ?></td>
                        <td><?php echo $Singledata['title'] ?></td>
                        <td><a href="show.php?id=<?php echo $Singledata['id'] ?>">View</a></td>
                        <td><a href="edit.php?id=<?php echo $Singledata['id'] ?>">Edit</a></td>
                        <td><a href="delete.php?id=<?php echo $Singledata['id'] ?>">Delete</a></td>

                    </tr>
                <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="3">
                        No available data
                    </td>
                </tr>
                <?php
            }
            ?>

        </table>
    </body>
</html>
